#+LaTeX_CLASS: book
#+TITLE: Django : Le Guide des performances
#+LaTeX: \setlength\parindent{0.0in}
#+LaTeX: \setlength\parskip{0.2in}
#+LaTeX_HEADER: \usepackage{framed}
#+LaTeX_HEADER: \usepackage{xcolor}
#+LaTeX_HEADER: \definecolor{shadecolor}{gray}{.95}
#+LaTeX_HEADER: \newenvironment{quote}{\begin{shaded}}{\end{shaded}}
* Introduction
* Django base de données et performance d'accès
** La gestion de la mémoire sur les listes d'objets
*** Le problème

Lorsque l'on travaille sur une liste d'objet provenant d'une QuerySet
on oublie souvent comment Django traite ces données en interne et cela
peut ammener à de grave disfonctionnements. En effet, par défaut
Django va charger en mémoire l'ensemble des lignes retournées par la
base de donnée après les avoir converti en objets Python. Dans le cas
d'un grand nombre de ligne, la consomation mémoire va devenir très
importante et peut causer des interuptions de services ou un
ralentissemet de l'ensemble de votre plateforme.

Voici un exemple pour 1000 articles:

#+BEGIN_SRC

Line     Mem usage    Increment   Line Contents

    10     28.4 MiB      0.0 MiB       @profile
    11                                 def handle(self, *args, **options):
    12     28.4 MiB      0.0 MiB           start = time()
    13     29.2 MiB      0.8 MiB           print Article.objects.count()
    14     46.8 MiB     17.6 MiB           for article in Article.objects.all():
    15     46.8 MiB      0.0 MiB               article.published_date = datetime.now()
    16     46.8 MiB      0.0 MiB               article.save()
    17     46.3 MiB     -0.5 MiB           print time() - start

36.4094641209 secondes

#+END_SRC

*** Solution 1: Utiliser update
Quand la ou les valeurs que vous allez mettre à jour est identique
pour chaque objets, il n'est pas utile de passer par le cycle:

- récupérer l'objet
- changer la valeur de l'attribut
- sauvegarder l'objet en base

Vous pouvez utiliser "update" qui va être beaucoup plus rapide et plus
simple à écrire.

#+BEGIN_SRC

Line #    Mem usage    Increment   Line Contents

    10     28.4 MiB      0.0 MiB       @profile
    11                                 def handle(self, *args, **options):
    12     28.4 MiB      0.0 MiB           start = time()
    13     29.2 MiB      0.8 MiB           print Article.objects.count()
    14     31.2 MiB      2.1 MiB           Article.objects.all().update(published_date=datetime.now())
    15     31.2 MiB      0.0 MiB           print time() - start

0.0745010375977 secondes

#+END_SRC

Django va utiliser la méthode UPDATE de votre base de donnée et faire
la modification en une seule fois pour tous vos objets. Cela va vous
faire gagner du temps et de l'occupation mémoire.

Seul point important, vous ne passerez pas par la méthode "save" et
ne déclencherez pas de signaux sur votre application.

*** Solution 2: Utiliser des slices

Quand vous ne pouvez pas utiliser update,

- soit parce que la valeur du champ à modifier est différente pour
  chaque enregistrement

- vous avez besoin de passer par la méthode save

- vous ayez besoin d'emmettre un signal

Par exemple:

#+BEGIN_SRC

Line #    Mem usage    Increment   Line Contents

    10     28.4 MiB      0.0 MiB       @profile
    11                                 def handle(self, *args, **options):
    12     28.4 MiB      0.0 MiB           start = time()
    13     29.1 MiB      0.7 MiB           print Article.objects.count()
    14     46.8 MiB     17.7 MiB           for article in Article.objects.all():
    15     46.8 MiB      0.0 MiB               article.view_count = random.randint(0, 1000)
    16     46.8 MiB      0.0 MiB               article.save()
    17     46.3 MiB     -0.5 MiB           print time() - start

35.1801760197 secondes

#+END_SRC

Comme vous le voyez, vous êtes de nouveau dans le premier cas : c'est
long et c'est couteux en mémoire.

**** Utiliser LIMIT et OFFSET

La première solution consiste à faire des slices pour consommer le
moins de mémoire possible:

#+BEGIN_SRC

Line #    Mem usage    Increment   Line Contents

    10     30.5 MiB      0.0 MiB       @profile
    11                                 def handle(self, *args, **options):
    12     30.5 MiB      0.0 MiB           start_time = time()
    13     31.1 MiB      0.6 MiB           count = Article.objects.count()
    14     31.1 MiB      0.0 MiB           print count
    15     31.1 MiB      0.0 MiB           start = 0
    16     31.1 MiB      0.0 MiB           step = min(100, count)
    17     32.2 MiB      1.1 MiB           while count > step:
    18     32.2 MiB      0.0 MiB               for article in Article.objects.all()[start:step]:
    19     32.2 MiB      0.0 MiB                   article.view_count = random.randint(0, 1000)
    20     32.2 MiB      0.0 MiB                   article.save()
    21     32.2 MiB      0.0 MiB               step += 100
    22     32.2 MiB      0.0 MiB               start += 100
    23     32.2 MiB      0.0 MiB           print time() - start_time

14.4458150864 secondes

#+END_SRC

Le problème avec cette solution c'est que vous utilisez à la fois un
LIMIT et un OFFSET. Quand vous utilisez OFFSET, votre base de donnée
doit parcourir toute votre table et ne récupérer que les éléments
siutés entre l'OFFSET et le LIMIT ce qui est couteux.

**** utiliser seulement LIMIT

Vous pouvez faire en sorte de ne pas utiliser d'OFFSET en utilisant
les "id" de vos modèles :
#+BEGIN_SRC

Line #    Mem usage    Increment   Line Contents

    10     30.8 MiB      0.0 MiB       @profile
    11                                 def handle(self, *args, **options):
    12     30.8 MiB      0.0 MiB           start_time = time()
    13     31.3 MiB      0.5 MiB           count = Article.objects.count()
    14     31.3 MiB      0.0 MiB           print count
    15     31.3 MiB      0.0 MiB           start = Article.objects.all().order_by("pk").first().pk - 1
    16     32.3 MiB      1.0 MiB           while True:
    17     32.3 MiB      0.0 MiB               articles = Article.objects.filter(pk__gt=start)[:100]
    18     32.3 MiB      0.0 MiB               if articles:
    19     32.3 MiB      0.0 MiB                   for article in articles:
    20     32.3 MiB      0.0 MiB                       article.view_count = random.randint(0, 1000)
    21     32.3 MiB      0.0 MiB                       article.save()
    22     32.3 MiB      0.0 MiB                   start = article.pk
    23                                         else:
    24     32.3 MiB      0.0 MiB                   break
    25     32.3 MiB      0.0 MiB           print time() - start_time

6.46871209145 secondes

#+END_SRC

L'empreinte mémoire est sensiblement la même dans les deux versions,
mais la seconde est bien plus rapide car vous n'avez plus besoin
d'utiliser OFFSET.

** Les index
*** De l'interet des index dans les order_by
**** Order_by par la clé primaire
Immaginez que vous souhaitez ordonner vos résultats par ordre d'id
croissant:

voici votre modèle Article:

#+BEGIN_SRC python

class Article(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    author = models.ForeignKey(Author)
    created_date = models.DateField(auto_now_add=True)
    published_date = models.DateField(null=True, blank=True)
    last_modified = models.DateField(auto_now=True)
    published = models.BooleanField(default=False)
    view_count = models.IntegerField(default=0)

#+END_SRC

Et voici votre requête:

#+BEGIN_SRC python

>>> Article.objects.all().order_by("id")[:10]

#+END_SRC

#+BEGIN_SRC sql
(0.007) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article"
        ORDER BY "blog_article"."id" ASC LIMIT 10; args=()
#+END_SRC

Sur 21.000 articles, c'est assez rapide. Essayons maintenant
d'ordonner sur "last_modified":

**** order_by sur un champ sans index

#+BEGIN_SRC python

>>> Article.objects.all().order_by("last_modified")[:10]

#+END_SRC

#+BEGIN_SRC sql

(0.039) SELECT "blog_article"."id", "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article"
        ORDER BY "blog_article"."last_modified" ASC LIMIT 10;

#+END_SRC

Le temps qu'il faut pour trier sur un champ sans index est bien plus
long que sur la clé primaire.

**** Les indexes simples

Vous remarquez que le temps de tri sur la clé primaire est bien plus
rapide que sur "last_modified". En créant un index sur last_modified
vous pouvez améliorer les choses:

#+BEGIN_SRC python

class Article(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    author = models.ForeignKey(Author)
    created_date = models.DateField(auto_now_add=True)
    published_date = models.DateField(null=True, blank=True)
    last_modified = models.DateField(auto_now=True, db_index=True)
    published = models.BooleanField(default=False)
    view_count = models.IntegerField(default=0)

#+END_SRC

#+BEGIN_SRC python

>>> Article.objects.all().order_by("last_modified")[:10]

#+END_SRC

#+BEGIN_SRC sql

(0.003) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article"
        ORDER BY "blog_article"."last_modified" ASC LIMIT 10;
#+END_SRC

Avec l'index, vous retrouvez des performances acceptables.

**** les indexes sur plusieurs champs

Comme vous savez avoir de bonnes performances sur un tri sur le champ
"id" et le champ "last_modified", pourquoi ne pas trier sur les deux
champs à la fois. Par exemple :

#+BEGIN_SRC python

>>> Article.objects.all().order_by("last_modified", "id")[:10]

#+END_SRC

#+BEGIN_SRC sql

(0.042) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article"
        ORDER BY "blog_article"."last_modified" ASC,
                 "blog_article"."id" ASC LIMIT 10;
#+END_SRC

Visiblement vous n'avez pas les résultats attendus. Mais cette fois
aussi, une bonne utilisation des indexes peut vous aider grandement:

#+BEGIN_SRC python

class Article(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    author = models.ForeignKey(Author)
    created_date = models.DateField(auto_now_add=True)
    published_date = models.DateField(null=True, blank=True)
    last_modified = models.DateField(auto_now=True)
    published = models.BooleanField(default=False)
    view_count = models.IntegerField(default=0)

    class Meta:
        index_together = [
            ["last_modified", "id"]
        ]

#+END_SRC

#+BEGIN_SRC sql

(0.001) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article"
        ORDER BY "blog_article"."last_modified" ASC,
                 "blog_article"."id" ASC LIMIT 10;
#+END_SRC

*** Les indexes et la pagination
**** La pagination Django sans order_by
Quand il sagit de faire de la pagination dans une vue, vous utilisez
certainement le module de pagination prévu par Django.

Voyons voir comment il se comporte:

#+BEGIN_SRC python
from django.core.paginator import Paginator
from blog.models import Article

qs = Article.objects.all()
paginator = Paginator(qs, 10)
list(paginator.page(1).object_list)
#+END_SRC

#+BEGIN_SRC sql

(0.008) SELECT COUNT(*) FROM "blog_article"; args=()
(0.001) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article" LIMIT 10;
#+END_SRC

La pagination en elle même est assez rapide néanmoins Django fais un
count pour calculer le nombre de page.

Voyons comment il se comporte pour la seconde page:

#+BEGIN_SRC python

list(paginator.page(2).object_list)

#+END_SRC

#+BEGIN_SRC sql

(0.001) SELECT "blog_article"."id",
               "blog_article"."title",
               "blog_article"."content",
               "blog_article"."author_id",
               "blog_article"."created_date",
               "blog_article"."published_date",
               "blog_article"."last_modified",
               "blog_article"."published",
               "blog_article"."view_count"
        FROM "blog_article" LIMIT 10 OFFSET 10;

#+END_SRC

Cette fois Django a gardé en cache le nombre d'éléments dans la liste
et ne refais pas de count.

**** La pagination Django avec order_by
Maintenant, il faut tester la pagination Django avec une clause
order_by et voir en quoi cela affecte les performances.

Pour ce tests, nous allons créer un paginateur :

#+BEGIN_SRC python
qs = Article.objects.order_by("-last_modified", "-id")
paginator = Paginator(qs, 100)
#+END_SRC

Puis, nous allons itérer sur toutes les pages et enregistrer le temps
passé en sql à chaque itération:

[[file:pagination_offset.png]]

Plus on s'éloigne de la première page, plus le temps de traitement
deviens long. Finalement, la dernière page est elle assez rapide
puisque la clause LIMIT n'as ici plus d'interet.

**** La pagination par clé

Il est possible d'obteni une pagination qui ne va pas devenir de plus
en plus lente au fil des pages. Cette technique de pagination est
dites pagination par clé.

Le projet "django-infinite-scroll-page" implémente cette technique
(https://github.com/nitely/django-infinite-scroll-pagination)

Nous allons la tester dans les mêmes condition que la pagination
classique:

#+BEGIN_SRC python

from infinite_scroll_pagination.paginator import SeekPaginator
from blog.models import Article

qs = Article.objects.all()
paginator = SeekPaginator(qs, 100,
                          lookup_field="last_modified")
obj = paginator.page(pk=None, value=None).object_list[-1]
while True:
    try:
        obj_list = list(
            paginator.page(pk=obj.pk,
                           value=obj.last_modified
                       ).object_list)
        obj = obj_list[-1]
    except:
        break

#+END_SRC

#+PLOT: title:"example table" ind:2 type:2d with:lines
#+PLOT: labels:("first new label" "second column" "last column")
#+TBLNAME:org-plot-example-1
[[file:pagination_cle.png]]

La nouvelle méthode est plus rapide que la précédente. De plus, au fil
des pages le temps passé en base de donnée n'augmente pas de façon
linéaire comme avec la première méthode.

* Des API performantes avec PostgreSQL > 9.2

Avec l'arrivée de PostgreSQL 9.2, il existe une nouvelle
fonctionnalitée qui permet de crer des API disposants de performances
encore jamais égalées.

Mais avant d'expliquer ce que PostgreSQL peut apporter aux API
essayons de détailler comment se décompose une API Django.

Dans sa version la plus simple le cheminement est le suivant:

- votre application reçoit une request. par exemple /blog/api/articles/
  GET
- votre application décode la requête et va contacter votre base de
  données pour récupérer les lignes correspondant aux 10 premiers
  articles.
- Django va convertir ces lignes en objet python
- votre serializer va convertir ces objets python en JSON
- votre application va retourner un payload JSON.

Une API simplissime pourait se présenter comme suit:

#+BEGIN_SRC python
from blog.models import Article
from django.views.generic import ListView

from django.core import serializers
from django import http


class AJAXListMixin(object):

    def render_to_response(self, context, **kwargs):
        qs = context["object_list"]
        response = serializers.serialize('json', qs)
        return http.HttpResponse(response)


class ArticleView(AJAXListMixin, ListView):
    model = Article
    paginate_by = 10
#+END_SRC

*** Le défaut des serializers

Pour tester le temps de rendu du serializer, faisons une petite
experience. Il s'agit de mesurer le temps de serialization d'une liste
d'objet en augmentant petit à petit le nombre d'objets à serializer.

#+BEGIN_SRC python
    def render_to_response(self, context, **kwargs):
        qs = list(context["object_list"])
        start = time.time()
        response = serializers.serialize('json', qs)
        print time.time() - start
        return http.HttpResponse(response)

#+END_SRC

Et c'est seulement la valeur de paginated_by qui augmente. Vous
remarquerez égalment que l'on "list" la queeryset. Cela va permettre
de s'assurer que c'est bien la serialisation et seulement la
serialisation que l'on augmente.

[[file:serialization_python.png]]

Pour se faire une idée du temps passé à la serialisation dans cette
requête, voici les requêtes qu'a faites Django dans la cas le plus
lent, avec 1000 objets:

#+BEGIN_SRC sql
{u'time': u'0.006', u'sql': u'SELECT COUNT(*) FROM "blog_article"'}
{u'time': u'0.006', u'sql': u'SELECT "blog_article"."id",
                                     "blog_article"."title",
                                     "blog_article"."content",
                                     "blog_article"."author_id",
                                     "blog_article"."created_date",
                                     "blog_article"."published_date",
                                     "blog_article"."last_modified",
                                     "blog_article"."published",
                                     "blog_article"."view_count"
                              FROM "blog_article" LIMIT 1000'}
#+END_SRC

Malgré le count supplémentaire (il s'agit d'une pagination par OFFSET
et non d'une pagination par clé) le temps de serialisation est 200
fois plus long que le temps passé en base de donnée.

Il y as donc une marge de progression non négligeable.

*** Serialization depuis PostgreSQL

Pour ce test nous allons mesurer le temps nécessaire pour faire une
requête à PostgreSQL en utilisant son rendu JSON sans passer par le
serializer python.

Voici notre nouvelle vue:

#+BEGIN_SRC python

import time
from blog.models import Article
from django.views.generic import ListView

from django.core import serializers
from django import http
from django.db import connection
from psycopg2 import extras

extras.register_default_json(loads=lambda x: x)


class AJAXListMixin(object):

    def get_queryset(self):
        c = connection.cursor()
        c.execute("""
        select array_to_json(array_agg(row_to_json(t)))
        FROM(
        SELECT * FROM blog_article LIMIT 10) t """)
        return c.fetchone()

    def render_to_response(self, context, **kwargs):
        start = time.time()
        response = http.HttpResponse(self.get_queryset())
        for t in connection.queries:
            print t
        print time.time() - start
        return response

class ArticleView(AJAXListMixin, ListView):
    model = Article
    paginate_by = 10

#+END_SRC

[[file:serialization_postgresql.png]]

Dans le pire cas, avec 1000 objets à serializer, on obtient un
temps de 0.035 secondes contre 0.25 secondes dans le cas de la
serialization python soit un gain de près de 10/1.

*** Serialization depuis un manager

Evidement cette méthode si elle possède des avantages évidents
présente aussi l'inconvénient de n'offrir que peu de souplesse. Vous
devez écrire votre requête à la main, et court-circuiter Django.

Il est pourtant possible, à l'aide d'un manager Django de profiter de
cette rapidité sans pour autant perdre des avantages de la souplesse
de Django.

Pour ce faire il faut écrire un manager :

#+BEGIN_SRC python

from django.db import models
from django.db import connection
from psycopg2 import extras
extras.register_default_json(loads=lambda x: x)


class PostgreSQLManager(models.Manager):

    def get_queryset(self):
        return PostgreSQLQuerySet(self.model, using=self._db)

    def to_json(self):
        return self.get_queryset().to_json()


class PostgreSQLQuerySet(models.QuerySet):

    def to_json(self):
        template = """
        select array_to_json(array_agg(row_to_json(t)))
        FROM({}) t"""
        sql = self.query.sql_with_params()
        args = (template.format(sql[0]),
                sql[1])
        with connection.cursor() as c:
            c.execute(*args)
            result = c.fetchone()[0]
        return result

#+END_SRC

A partir de maintenant, tous les models qui utiliseront ce manager
auont la possiblité d'appeller la méthode to_json sur quelque QuerySet
que ce soit.

Ajoutons donc ce manager a notre modele Article:

#+BEGIN_SRC python

from django.db import models
from manager import PostgreSQLManager

class Article(models.Model):
    title = models.CharField(max_length=250)
    content = models.TextField()
    author = models.ForeignKey(Author)
    created_date = models.DateField(auto_now_add=True)
    published_date = models.DateField(null=True, blank=True)
    last_modified = models.DateField(auto_now=True)
    published = models.BooleanField(default=False)
    view_count = models.IntegerField(default=0)

    objects = PostgreSQLManager()

#+END_SRC

maintenant que votre modèle est capable de rendre une queryset en
json, essayez le !

#+BEGIN_SRC python

from blog.models import Article
from django.views.generic import ListView
from django import http


class AJAXListMixin(object):

    def get_context_data(self, **kwargs):
        context = super(AJAXListMixin, self).get_context_data(**kwargs)
        context['object_list'] = context['object_list'].to_json()
        return context

    def render_to_response(self, context, **kwargs):
        return http.HttpResponse(context['object_list'])


class ArticleView(AJAXListMixin, ListView):
    model = Article
    paginate_by = 10


#+END_SRC

Avec très peu de code vous profitez maintenant du rendu JSON de
PostgreSQL sans perdre les avantages de Django.

Vous constaterez que la pagination par défaut de Django fonctionne à
merveille, que vous pouvez ajouter des filtres, des agrégations, bref
utiliser cette nouvelle méthode pour absoluement toutes les QuerySet
que vous voulez.

** Serialization PostgreSQL avec django_restframework
*** Les Vues RestFramework
Django RestFramework est un moteur d'API très puissant pour
Django. C'est à l'heure d'écrire ces lignes le framework d'API REST le
plus utilisé pour Django.

L'idée ici va être de voir comment, avec ce que vous venez de
développer, vous pouvez utiliser restframework avecle serialiser JSON
de postgreSQL.

Tout d'abord, installez django-restframework:

#+BEGIN_SRC sh
pip install djangorestframework
#+END_SRC

Ensuite écrivez un serializer:

#+BEGIN_SRC python

#blog/serializers.py
from blog.models import Article
from rest_framework import serializers


class ArticleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Article
        fields = ('id', 'title', 'content', 'created_date',
                  'published_date', 'last_modified', 'published',
                  'view_count')

#+END_SRC

Puis écrivez votre première vue:

#+BEGIN_SRC python

#blog/views.py
from blog.models import Article

from django.db import connection
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from serializers import ArticleSerializer


class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


@csrf_exempt
def article_list(request):
    """
    List all articles, or create a new one.
    """
    if request.method == 'GET':
        articles = Article.objects.all()
        serializer = ArticleSerializer(articles, many=True)
        return JSONResponse(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = ArticleSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)
        return JSONResponse(serializer.errors, status=400)


@csrf_exempt
def article_detail(request, pk):
    """
    Retrieve, update or delete an article .
    """
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ArticleSerializer(article)
        return JSONResponse(JSONRenderer().render(serializer.data))

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ArticleSerializer(article, data=data)
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data)
        return JSONResponse(serializer.errors, status=400)

    elif request.method == 'DELETE':
        article.delete()
        return HttpResponse(status=204)


#+END_SRC

Vous pouvez ensuite lier cette vue à vos urls:

#+BEGIN_SRC python

from django.conf.urls import url
from blog import views

urlpatterns = [
    url(r'^articles/$', views.article_list),
    url(r'^articles/(?P<pk>[0-9]+)/$', views.article_detail),
]

#+END_SRC


vous pouvez maintenant tester votre api:

#+BEGIN_SRC sh
curl -i http://localhost:8000/blog/articles/
[
    {
        "content": "Lorem ipsum dolor sit amet, ...",
        "created_date": "2015-02-08",
        "id": 4763,
        "last_modified": "2014-09-07",
        "published": false,
        "published_date": "2013-03-07",
        "title": "Temporibus tenetur cum officia ...",
        "view_count": 0
    },
    {
        "content": "Lorem ipsum dolor sit amet, ...",
        "created_date": "2015-02-08",
        "id": 4764,
        "last_modified": "2014-09-05",
        "published": false,
        "published_date": "2010-03-17",
        "title": "Nostrum a est, autem non voluptatibus ...",
        "view_count": 0
    },
    ...
]

#+END_SRC

*** Le rendu JSON PostgreSQL sur la vue article_list

Bien que cette API soit pour le moins sommaire, vous pouvez déjà
grandement l'améliorer.

Dans un premier temps, il faut reprendre votre serializer pour qu'il
soit en mesure de renvoyer directement du json plutôt qu'une liste
d'OrderdDict qui est son comportement par défaut.

#+BEGIN_SRC python

from blog.models import Article
from rest_framework import serializers


class ArticleListSerializer(serializers.ListSerializer):

    def to_representation(self, data):
        return data.values(
            *(k for k in self.child.fields.iterkeys())
        ).to_json()


class ArticleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Article
        list_serializer_class = ArticleListSerializer
        fields = ('id', 'title', 'content', 'created_date',
                  'published_date', 'last_modified', 'published',
                  'view_count')

#+END_SRC

Maintenant que votre serializer renvois du JSON, vous n'avez plus
besoin, dans votre vue du JSONRenderer. Vous pouvez donc écrire:

#+BEGIN_SRC python

class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = data
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

#+END_SRC

En testant votre API, vous pourrez constater que le temps de rendu est
beaucoup plus court.

Vous pouvez également modifier l'attribut "fields" de la class Meta de
votre ArticleSerializer et constater que votre API répond correctement
aux changements.

*** Utiliser le rendu par défaut pour article_detail

Vous avez peut-être constaté que votre vue detail ne fonctionne
plus. En effet, comme vous avez débranché le serializer JSON de votre
class JSONResponse, vous ne pouvez plus l'utiliser pour la vue détail.

Fort heureusement c'est très simple à réimplémenter:

#+BEGIN_SRC python

@csrf_exempt
def article_detail(request, pk):
    """
    Retrieve, update or delete an article .
    """
    try:
        article = Article.objects.get(pk=pk)
    except Article.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == 'GET':
        serializer = ArticleSerializer(article)
        return JSONResponse(JSONRenderer().render(serializer.data))

#+END_SRC

en utilisant JSONRenderer().render dans la vue détail, vous retrouvez
le fonctionnement normal de restframework.

*** conclusion

Avec quelques étapes simples vous êtes en mesure d'utiliser le rendu
JSON PostgreSQL quand cela vous permet d'obtenir un gain de
performance mais d'utiliser toutes les fonctionalités et la souplesse
de restframework pour les autres cas.

Dans le prochain exemple vous allez utiliser les vues génériques
restframework de la même manière qu'avec ce cas simple.

* Conclusion
